import gi, alarm, timer, stopwatch
gi.require_version("Gtk", "3.0")
gi.require_version("Handy", "1")
from gi.repository import Gtk



class Main(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_default_size(400,400)

        self.header = Gtk.HeaderBar()
        self.header.props.show_close_button = True
        self.set_titlebar(self.header)
        self.mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)

        self.stack = Gtk.Stack()
        self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.stack.set_transition_duration(1000)
        self.stack.add_titled(alarm.AlarmWindow(self), "alarm", "Alarm")
        self.stack.add_titled(timer.TimerWindow(self), "timer", "Timer")
        self.stack.add_titled(stopwatch.StopwatchWindow(self), "stopwatch", "Stopwatch")

        self.stack_switcher = Gtk.StackSwitcher()
        self.stack_switcher.set_stack(self.stack)
        #self.stack_switcher.set_halign(Gtk.Align.CENTER)
        #self.stack.set_halign(Gtk.Align.CENTER)
        self.header.set_custom_title(self.stack_switcher)
        self.mainbox.pack_start(self.stack, True, True, 0)
        self.add(self.mainbox)

        self.show_all()



if __name__ == "__main__":
    main = Main()
    main.connect("destroy", Gtk.main_quit)
    Gtk.main()
