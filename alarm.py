import gi, time, update_settings, threading
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from playsound import playsound

class AlarmWindow(Gtk.ScrolledWindow):

    def __init__(self, parent):
        super().__init__()
        style = Gtk.CssProvider()
        style.load_from_path("look.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.parent = parent
        self.mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(self.mainbox)

        self.settings = update_settings.read_settings()
        self.alarms = self.settings["Alarms"]

        label = Gtk.Image(stock=Gtk.STOCK_ADD)
        button = Gtk.Button()
        button.set_image(label)
        button.connect("clicked", self.create_alarm)
        self.alarm_box = Gtk.ListBox()
        self.mainbox.add(self.alarm_box)
        self.mainbox.add(button)

        if len(self.alarms) > 0:
            for name in self.alarms:
                data = [name, self.alarms[name][0],self.alarms[name][1],self.alarms[name][2]]
                self.alarm_row(data)

        self.show_all()


    def create_alarm(self, widget):
        new = NewAlarm(self.parent)
        response = new.run()

        if response == Gtk.ResponseType.OK:
            output = [new.name.get_text(), self.checknew.hour.get_value_as_int(), new.minute.get_value_as_int(), new.second.get_value_as_int()]
            self.alarm_row(output)
            self.settings["Alarms"][output[0]] = output[1:]
            update_settings.write_settings(self.settings)
        new.destroy()


    def alarm_row(self, data):

        for number in data[1:]:
            index = data.index(number)
            if len(str(number)) < 2:
                data[index] = f"0{number}"
        new_row = Gtk.ListBoxRow()
        row_box = Gtk.Box()
        new_row.add(row_box)
        alarm_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        row_box.add(alarm_box)
        name = Gtk.Label()
        name.set_markup(f"<small>{data[0]}</small>")
        time = Gtk.Label()
        time.set_markup(f"<big>{data[1]}:{data[2]}:{data[3]}</big>")
        alarm_box.add(name)
        alarm_box.add(time)
        new_box = Gtk.Box()
        label = Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)
        play_button = Gtk.Button()
        play_button.connect("clicked", lambda widget: Alarm(data[0], [data[1],data[2],data[3]], self.parent))
        play_button.set_image(label)
        label = Gtk.Image(stock=Gtk.STOCK_REMOVE)
        remove_button = Gtk.Button()
        remove_button.connect("clicked", lambda widget: self.destroy(new_row, data[0]))
        remove_button.set_image(label)
        new_box.add(play_button)
        new_box.add(remove_button)
        row_box.pack_end(new_box, False, False, 0)
        self.alarm_box.add(new_row)
        self.show_all()


    def destroy(self, row, name):
        confirmation = Warning(self.parent)
        response = confirmation.run()
        if response == Gtk.ResponseType.OK:
            row.destroy()
            self.settings["Alarms"].pop(name)
            update_settings.write_settings(self.settings)
        confirmation.destroy()

class Warning(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Warning", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_NO, Gtk.ResponseType.CANCEL, Gtk.STOCK_YES, Gtk.ResponseType.OK
        )
        self.set_default_size(300,100)
        titlebar = Gtk.HeaderBar(title="Warning")
        self.set_titlebar(titlebar)
        box = self.get_content_area()
        warninglabel = Gtk.Label(label="Are you sure you want to remove this?")
        box.add(warninglabel)
        self.show_all()


class NewAlarm(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="New Alarm", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(300, 100)

        titlebar = Gtk.HeaderBar(title="New Alarm")
        self.set_titlebar(titlebar)

        mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        spinbox = Gtk.Box(spacing=10)

        self.hour_label = Gtk.Label(label="Hour:")
        self.minute_label = Gtk.Label(label="Minute:")
        self.second_label = Gtk.Label(label="Second:")
        self.name = Gtk.Entry()
        self.name.set_text("New Alarm")
        self.houradj = Gtk.Adjustment(upper=24, step_increment=1, page_increment=1)
        self.minuteadj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.secondadj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.hour = Gtk.SpinButton()
        self.hour.set_adjustment(self.houradj)
        self.minute = Gtk.SpinButton()
        self.minute.set_adjustment(self.minuteadj)
        self.second = Gtk.SpinButton()
        self.second.set_adjustment(self.secondadj)

        spinbox.add(self.hour_label)
        spinbox.pack_start(self.hour, True, True, 0)
        mainbox.add(spinbox)
        spinbox = Gtk.Box(spacing=10)
        spinbox.add(self.minute_label)
        spinbox.pack_start(self.minute, True, True, 0)
        mainbox.add(spinbox)
        spinbox = Gtk.Box(spacing=10)
        spinbox.add(self.second_label)
        spinbox.pack_start(self.second, True, True, 0)
        mainbox.add(spinbox)

        box = self.get_content_area()
        box.add(self.name)
        box.add(mainbox)
        self.show_all()

class Alarm(Gtk.Dialog):
    def __init__(self, name, alarm, parent):
        super().__init__(title=name, transient_for=parent, flags=0)
        #self.set_default_size(300,300)
        self.alarm = alarm
        self.bool_list = ["", "", ""]
        self.check_time()
        self.pause_bool = False
        header = Gtk.HeaderBar(title=name)
        self.set_titlebar(header)
        mainbox = self.get_content_area()
        self.alarmbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.alarmlabel = Gtk.Label(label=f"{self.bool_list[0]}{self.alarm[0]}:{self.bool_list[1]}{self.alarm[1]}:{self.bool_list[2]}{self.alarm[2]}")
        self.timelabel = Gtk.Label(label=time.strftime("%H:%M:%S"))
        self.timelabel.set_name("time")
        self.alarmbox.pack_start(self.alarmlabel, True, True, 0)
        self.alarmbox.pack_start(self.timelabel, True, True, 0)
        button_box = Gtk.Box()
        button_box.set_halign(Gtk.Align.CENTER)
        self.pauselabel = Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE)
        cancellabel = Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP)
        cancel_button = Gtk.Button()
        cancel_button.set_image(cancellabel)
        cancel_button.connect("clicked", self.stop)
        button_box.add(cancel_button)
        mainbox.add(self.alarmbox)
        mainbox.add(button_box)
        self.show_all()
        self.countdown = threading.Thread(target=self.start, args=(self.alarm,))
        self.countdown.daemon = True
        self.countdown.start()


    def stop(self, widget):
        self.pause_bool = True
        self.destroy()

    def check_time(self):
        if self.alarm[0] < 10:
            self.bool_list[0] = "0"
        else:
            self.bool_list[0] = ""
        if self.alarm[1] < 10:
            self.bool_list[1] = "0"
        else:
            self.bool_list[1] = ""
        if self.alarm[2] < 10:
            self.bool_list[2] = "0"
        else:
            self.bool_list[2] = ""

    def start(self, alarm):
        while True:
            self.check_time()
            hour = time.strftime("%H")
            minute = time.strftime("%M")
            second = time.strftime("%S")
            self.alarmlabel.set_text(f"{self.bool_list[0]}{self.alarm[0]}:{self.bool_list[1]}{self.alarm[1]}:{self.bool_list[2]}{self.alarm[2]}")
            self.timelabel.set_text(str(time.strftime("%H:%M:%S")))
            if hour == f"{self.bool_list[0]}{self.alarm[0]}" and minute == f"{self.bool_list[1]}{self.alarm[1]}" and second == f"{self.bool_list[2]}{self.alarm[2]}":
                playsound("alarm.mp3")
                break
            time.sleep(1)

            if self.pause_bool:
                break

#alarm = AlarmWindow()
#alarm.connect("destroy", Gtk.main_quit)
#Gtk.main()
