import json, pathlib
home = pathlib.Path.home()
def write_settings(data):
    settings_file = open(f"{home}/.config/ultimer.json", "w")
    json.dump(data, settings_file, indent=5)
    settings_file.close()

def read_settings():
    settings_file = open(f"{home}/.config/ultimer.json", "r")
    data = json.load(settings_file)
    settings_file.close()
    return data
