import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Window(Gtk.Window):

    def __init__(self):
        super().__init__()

    def main(self):
        self.set_border_width(10)

        self.header = Gtk.HeaderBar(title="New Timer")
        self.set_titlebar(self.header)
        self.box = Gtk.Box(spacing=10)
        self.add(self.box)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)

        self.spinbox = Gtk.Box()
        self.box.pack_start(self.spinbox, True, True, 0)

        self.hour_label = Gtk.Label(label="Hour:")
        self.minute_label = Gtk.Label(label="Minute:")
        self.second_label = Gtk.Label(label="Second:")
        self.name = Gtk.Entry()
        self.houradj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.minuteadj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.secondadj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.hour = Gtk.SpinButton()
        self.hour.set_adjustment(self.houradj)
        self.minute = Gtk.SpinButton()
        self.minute.set_adjustment(self.minuteadj)
        self.second = Gtk.SpinButton()
        self.second.set_adjustment(self.secondadj)
        self.spinbox.pack_start(self.hour, False, False, 0)
        self.spinbox.pack_start(self.minute, False, False, 0)
        self.spinbox.pack_start(self.second, False, False, 0)

        self.button_box = Gtk.Box()
        self.box.pack_start(self.button_box, True, True, 0)

        self.done = Gtk.Button(label="Done")
        self.cancel = Gtk.Button(label="Cancel")
        self.cancel.connect("clicked", Gtk.main_quit)
        self.button_box.pack_start(self.done, True, True, 0)
        self.button_box.pack_start(self.cancel, True, True, 0)

        self.show_all()

    def return_digits(self):
        return self.hour.get_value_as_int(),self.minute.get_value_as_int(),self.second.get_value_as_int()




#new_timer = Window()
#new_timer.connect("destroy", Gtk.main_quit)
#new_timer.main()
#Gtk.main()
#print(new_timer.return_digits())
