import gi, time, update_settings, threading
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from playsound import playsound

class TimerWindow(Gtk.ScrolledWindow):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        style = Gtk.CssProvider()
        style.load_from_path("look.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.settings = update_settings.read_settings()
        self.timers = self.settings["Timers"]

        self.mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(self.mainbox)

        label = Gtk.Image(stock=Gtk.STOCK_ADD)
        button = Gtk.Button()
        button.set_image(label)
        button.connect("clicked", self.create_timer)
        self.timer_box = Gtk.ListBox()
        self.mainbox.add(self.timer_box)
        self.mainbox.add(button)

        if len(self.timers) > 0:
            for name in self.timers:
                data = [name, self.timers[name][0],self.timers[name][1],self.timers[name][2]]

                self.timer_row(data)
        else:
            pass
        self.show_all()

    def create_timer(self, widget):
        new = NewTimer(self.parent)
        response = new.run()

        if response == Gtk.ResponseType.OK:
            output = [new.name.get_text(), new.hour.get_value_as_int(), new.minute.get_value_as_int(), new.second.get_value_as_int()]
            self.timer_row(output)
            self.settings["Timers"][output[0]] = output[1:]
            update_settings.write_settings(self.settings)
        new.destroy()

    def timer_row(self, data):
        new_row = Gtk.ListBoxRow()
        row_box = Gtk.Box()
        new_row.add(row_box)
        timer_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        row_box.add(timer_box)
        name = Gtk.Label()
        name.set_markup(f"<small>{data[0]}</small>")
        time = Gtk.Label()
        time.set_markup(f"<big>{data[1]}:{data[2]}:{data[3]}</big>")
        timer_box.add(name)
        timer_box.add(time)
        new_box = Gtk.Box()
        label = Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)
        play_button = Gtk.Button()
        play_button.connect("clicked", lambda widget: Timer(data[0], [data[1],data[2],data[3]], self.parent))
        play_button.set_image(label)
        label = Gtk.Image(stock=Gtk.STOCK_REMOVE)
        remove_button = Gtk.Button()
        remove_button.connect("clicked", lambda widget: self.destroy(new_row, data[0]))
        remove_button.set_image(label)
        new_box.add(play_button)
        new_box.add(remove_button)
        row_box.pack_end(new_box, False, False, 0)
        self.timer_box.add(new_row)
        self.show_all()


    def destroy(self, row, name):
        confirmation = Warning(self.parent)
        response = confirmation.run()
        if response == Gtk.ResponseType.OK:
            row.destroy()
            self.settings["Timers"].pop(name)
            update_settings.write_settings(self.settings)
        confirmation.destroy()

class Warning(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Warning", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_NO, Gtk.ResponseType.CANCEL, Gtk.STOCK_YES, Gtk.ResponseType.OK
        )
        self.set_default_size(300,100)
        titlebar = Gtk.HeaderBar(title="Warning")
        self.set_titlebar(titlebar)
        box = self.get_content_area()
        warninglabel = Gtk.Label(label="Are you sure you want to remove this?")
        box.add(warninglabel)
        self.show_all()


class NewTimer(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="New Timer", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(300, 100)

        titlebar = Gtk.HeaderBar(title="New Timer")
        self.set_titlebar(titlebar)

        mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        spinbox = Gtk.Box(spacing=10)

        self.hour_label = Gtk.Label(label="Hour:")
        self.minute_label = Gtk.Label(label="Minute:")
        self.second_label = Gtk.Label(label="Second:")
        self.name = Gtk.Entry()
        self.name.set_text("New Timer")
        self.houradj = Gtk.Adjustment(upper=24, step_increment=1, page_increment=1)
        self.minuteadj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.secondadj = Gtk.Adjustment(upper=60, step_increment=1, page_increment=1)
        self.hour = Gtk.SpinButton()
        self.hour.set_adjustment(self.houradj)
        self.minute = Gtk.SpinButton()
        self.minute.set_adjustment(self.minuteadj)
        self.second = Gtk.SpinButton()
        self.second.set_adjustment(self.secondadj)

        spinbox.add(self.hour_label)
        spinbox.pack_start(self.hour, True, True, 0)
        mainbox.add(spinbox)
        spinbox = Gtk.Box(spacing=10)
        spinbox.add(self.minute_label)
        spinbox.pack_start(self.minute, True, True, 0)
        mainbox.add(spinbox)
        spinbox = Gtk.Box(spacing=10)
        spinbox.add(self.second_label)
        spinbox.pack_start(self.second, True, True, 0)
        mainbox.add(spinbox)

        box = self.get_content_area()
        box.add(self.name)
        box.add(mainbox)
        self.show_all()

class Timer(Gtk.Dialog):
    def __init__(self, name, timer, parent):
        super().__init__(title=name, transient_for=parent, flags=0)
        #self.set_default_size(300,300)
        self.timer = timer
        self.bool_list = ["", "", ""]
        self.check_time()
        self.pause_bool = False
        header = Gtk.HeaderBar(title=name)
        self.set_titlebar(header)
        mainbox = self.get_content_area()
        self.timerbox = Gtk.Box()
        self.timerlabel = Gtk.Label(label=f"{self.bool_list[0]}{self.timer[0]}:{self.bool_list[1]}{self.timer[1]}:{self.bool_list[2]}{self.timer[2]}")
        self.timerlabel.set_name("time")
        self.timerbox.pack_start(self.timerlabel, True, True, 0)
        button_box = Gtk.Box()
        button_box.set_halign(Gtk.Align.CENTER)
        self.pauselabel = Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE)
        cancellabel = Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP)
        self.pause_button = Gtk.Button()
        self.pause_button.connect("clicked", self.pause)
        self.pause_button.set_image(self.pauselabel)
        cancel_button = Gtk.Button()
        cancel_button.set_image(cancellabel)
        cancel_button.connect("clicked", lambda widget: self.destroy())
        button_box.add(self.pause_button)
        button_box.add(cancel_button)
        mainbox.add(self.timerbox)
        mainbox.add(button_box)
        self.show_all()
        self.countdown = threading.Thread(target=self.start, args=(self.timer,))
        self.countdown.daemon = True
        self.countdown.start()

    def check_time(self):
        if self.timer[0] < 10:
            self.bool_list[0] = "0"
        else:
            self.bool_list[0] = ""
        if self.timer[1] < 10:
            self.bool_list[1] = "0"
        else:
            self.bool_list[1] = ""
        if self.timer[2] < 10:
            self.bool_list[2] = "0"
        else:
            self.bool_list[2] = ""

    def start(self, timer):
        while True:
            self.check_time()
            self.timerlabel.set_text(f"{self.bool_list[0]}{self.timer[0]}:{self.bool_list[1]}{self.timer[1]}:{self.bool_list[2]}{self.timer[2]}")
            if timer[0] == 0 and timer[1] == 0 and timer[2] == 0:
                while not self.pause_bool:
                    playsound("alarm.mp3")
                    break
            timer[2] -= 1
            if timer[2] == -1:
                timer[1] -= 1
                timer[2] = 59
                if timer[1] == -1:
                    timer[0] -= 1
                    timer[1] = 59
            time.sleep(1)
            if self.pause_bool:
                break


    def pause(self, widget):
        if self.pause_bool:
            self.pause_bool = False
            self.pauselabel = Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE)
            self.pause_button.set_image(self.pauselabel)
            self.countdown = threading.Thread(target=self.start, args=(self.timer,))
            self.countdown.daemon = True
            self.countdown.start()
        else:
            self.pause_bool = True
            self.pauselabel = Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)
            self.pause_button.set_image(self.pauselabel)

#timer = TimerWindow()
#timer.connect("destroy", Gtk.main_quit)
#Gtk.main()
