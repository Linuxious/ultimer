import gi, time, threading, glib
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

class StopwatchWindow(Gtk.Box):

    stopwatch = [0,0,0,0]
    bool_list = ["0", "0", "0", "0"]
    pause_bool = False

    def __init__(self, parent):
        super().__init__(orientation=Gtk.Orientation.VERTICAL)
        self.set_valign(Gtk.Align.CENTER)
        self.parent = parent
        style = Gtk.CssProvider()
        style.load_from_path("look.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        stopwatchbox = Gtk.Box()
        button_box = Gtk.Box()
        button_box.set_halign(Gtk.Align.CENTER)
        self.add(stopwatchbox)
        self.add(button_box)


        start_label = Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)
        pause_label = Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE)
        stop_label = Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP)

        start = Gtk.Button()
        start.set_image(start_label)
        start.connect("clicked", self.start)
        pause = Gtk.Button()
        pause.set_image(pause_label)
        pause.connect("clicked", self.pause)
        stop = Gtk.Button()
        stop.set_image(stop_label)
        stop.connect("clicked", self.stop)
        button_box.add(start)
        button_box.add(pause)
        button_box.add(stop)

        self.stopwatchlabel = Gtk.Label(label=f"{self.bool_list[0]}{self.stopwatch[0]}:{self.bool_list[1]}{self.stopwatch[1]}:{self.bool_list[2]}{self.stopwatch[2]}:{self.bool_list[3]}{self.stopwatch[3]}")
        self.stopwatchlabel.set_name("time")
        stopwatchbox.pack_start(self.stopwatchlabel, True, True, 0)
        self.show_all()


    def start(self, widget):

        self.countdown = threading.Thread(target=self.count, args=(self.stopwatch,))
        self.countdown.daemon = True
        self.countdown.start()

    def check_stopwatch(self):
        if self.stopwatch[0] < 10:
            self.bool_list[0] = "0"
        else:
            self.bool_list[0] = ""
        if self.stopwatch[1] < 10:
            self.bool_list[1] = "0"
        else:
            self.bool_list[1] = ""
        if self.stopwatch[2] < 10:
            self.bool_list[2] = "0"
        else:
            self.bool_list[2] = ""
        if self.stopwatch[3] < 10:
            self.bool_list[3] = "0"
        else:
            self.bool_list[3] = ""

    def count(self, stopwatch):
        if self.pause_bool:
            self.pause_bool = False
        while True:
            self.check_stopwatch()
            Gdk.threads_add_idle(1, self.stopwatchlabel.set_text, (f"{self.bool_list[0]}{self.stopwatch[0]}:{self.bool_list[1]}{self.stopwatch[1]}:{self.bool_list[2]}{self.stopwatch[2]}:{self.bool_list[3]}{self.stopwatch[3]}"))
            #self.stopwatchlabel.set_text(f"{self.bool_list[0]}{self.stopwatch[0]}:{self.bool_list[1]}{self.stopwatch[1]}:{self.bool_list[2]}{self.stopwatch[2]}:{self.bool_list[3]}{self.stopwatch[3]}")
            stopwatch[3] +=1
            if stopwatch[3] == 100:
                stopwatch[2] += 1
                stopwatch[3] = 0
                if stopwatch[2] == 60:
                    stopwatch[1] += 1
                    stopwatch[2] = 0
                    if stopwatch[1] == 60:
                        stopwatch[0] += 1
                        stopwatch[1] = 0
            time.sleep(.01)
            if self.pause_bool:
                break


    def pause(self, widget):
        self.pause_bool = True


    def stop(self, widget):
        self.pause(widget)
        self.stopwatch = [0,0,0,0]
        self.check_stopwatch()
        self.stopwatchlabel.set_text(f"{self.bool_list[0]}{self.stopwatch[0]}:{self.bool_list[1]}{self.stopwatch[1]}:{self.bool_list[2]}{self.stopwatch[2]}:{self.bool_list[3]}{self.stopwatch[3]}")

